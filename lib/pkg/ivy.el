(use-package ivy
  :demand t
  :diminish ivy-mode
  :config (ivy-mode 1))
(use-package counsel
  :demand t
  :after (ivy)
  :bind (("C-x C-f" . counsel-find-file)
         ("C-x b" . counsel-switch-buffer)
         ("C-c r" . counsel-rg))
  :diminish counsel-mode
  :config (counsel-mode 1))
(use-package swiper
  :after (ivy)
  :bind ("C-s" . swiper))

(defun +ivy-posframe-display-exwm (str) ""
       (ivy-posframe--display str #'posframe-handler-exwm-top-center))
(use-package ivy-posframe
  :demand t
  :diminish ivy-posframe-mode
  :after (ivy)
  :init 
  (advice-add 'posframe--set-frame-position :before
              (lambda (&rest args)
                (setq-local posframe--last-posframe-pixel-position nil)))
  (setq ivy-posframe-border-width 0
        ivy-posframe-parameters '((parent-frame . nil))
        ivy-posframe-font "Rec Mono Duotone"
        ivy-posframe-display-functions-alist
        '((complete-symbol . ivy-posframe-display-at-point)
          (t . +ivy-posframe-display-exwm)))
  :config (ivy-posframe-mode 1))

