(use-package avy
  :bind (("M-g g" . avy-goto-line)
         ("M-g c" . avy-goto-char)))
(use-package ace-window
  :bind ("C-z C-z" . ace-window)
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)
        aw-scope 'frame
        aw-background nil))
