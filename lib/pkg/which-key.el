(use-package which-key
  :diminish which-key-mode
  :config (which-key-mode 1))

;; (use-package which-key-posframe
;;   :after (which-key posframe)
;;   :diminish which-key-posframe-mode
;;   :init 
;;   (setq which-key-posframe-poshandler 'posframe-poshandler-frame-center)
;;   :config (which-key-posframe-mode 1))
