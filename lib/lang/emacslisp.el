(defun +emacs-lisp-mode-h () ""
       (prettify-symbols-mode t)
       (lispy-mode t))

(use-package lispy
  :hook ((emacs-lisp-mode . +emacs-lisp-mode-h)))
