(use-package projectile
  :diminish projectile-mode
  :custom ((projectile-completion-system "helm"))
  :bind-keymap ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/src")
    (setq projectile-project-search-path '("~/src")))
  (setq projectile-switch-project-action #'projectile-dired))
