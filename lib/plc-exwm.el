;; -*- lexical-binding: t; -*-
(defun --insert-into-list (list el n)
  "Insert into list LIST an element EL at index N."
  (let* ((padded-list (cons nil list)) (c (nthcdr n padded-list)))
    (setcdr c (cons el (cdr c))) (cdr padded-list)))

(defun +exwm-init-h () ""
       (exwm-workspace-switch-create 0))

(global-set-key (kbd "s-z g c") #'workspace-insert-at-right)
(global-set-key (kbd "s-z g r") #'workspace-delete)
(global-set-key (kbd "s-z g R") #'workspace-delete-current)

(defvar workspace--workspaces '(:main :browser :misc))
(defun workspace--names () "Return list of workspace names as strings."
       (mapcar (lambda (i) (substring (symbol-name i) 1)) workspace--workspaces))
(defun workspace--get-index (name) "Return workspace index by NAME."
       (cl-position (read (concat ":" name)) workspace--workspaces))
(defun workspace--get-name (index) "Return workspace name as string by INDEX."
       (substring (symbol-name (nth index workspace--workspaces)) 1))
(defun workspace--get-current () "Return current workspace name."
       (workspace--get-name exwm-workspace-current-index))

(defun workspace--insert (name &optional at-left) "Insert workspace named NAME with direction based on whether AT-LEFT is nil or not."
       (let* ((idx exwm-workspace-current-index))
         (exwm-workspace-add (+ idx (if (not at-left) 1 0)))
         (setq workspace--workspaces (--insert-into-list workspace--workspaces (read (concat ":" name)) (+ idx (if (not at-left) 1 0))))
         (workspace--update-bar)))
(defun workspace-insert-at-left (name) "Insert workspace named NAME at left of current workspace."
       (interactive
        (list (read-string "New workspace name: ")))
       (workspace--insert name t))
(defun workspace-insert-at-right (name) "Insert workspace named NAME at right of current workspace."
       (interactive
        (list (read-string "New workspace name: ")))
       (workspace--insert name nil))

(defun workspace-append (name) "Append new workspace named NAME at the end of the list."
       (interactive
        (list (read-string "New workspace name: ")))
       (exwm-workspace-add)
       (add-to-list 'workspace--workspaces (read (concat ":" name)) t)
       (workspace--update-bar))

(defun workspace-delete (name) "Delete workspace named NAME."
       (interactive
        (list (completing-read "Workspace to delete: " (workspace--names))))
       (posframe-delete-all)
       (if (eql name (workspace--get-current))
           (workspace-delete-current)
         (progn (exwm-workspace-delete (workspace--get-index name))
                (setq workspace--workspaces (remove (read (concat ":" name)) workspace--workspaces))))
       (workspace--update-bar))
(defun workspace-delete-current () "Delete current workspace."
       (interactive)
       (let ((current (workspace--get-current))
             (current-idx exwm-workspace-current-index))
         (posframe-delete-all)
         (exwm-workspace-switch (if (eql current-idx 0) (+ current-idx 1) (- current-idx 1)))
         (exwm-workspace-delete current-idx)
         (setq workspace--workspaces (remove (read (concat ":" current)) workspace--workspaces)))
       (workspace--update-bar))

(defun workspace-rename-current (name) "Rename current workspace to NAME."
       (interactive (list (read-string "Rename workspace to: ")))
       (setf (nth exwm-workspace-current-index workspace--workspaces) (read (concat ":" name)))
       (workspace--update-bar))

(defun workspace-swap-current (name) "Swap current workspace with workspace named NAME."
       (interactive
        (list (completing-read "Workspace to swap with: " (remove (workspace--get-current) (workspace--names)))))
       (let* ((index (workspace--get-index name))
              (current-idx exwm-workspace-current-index)
              (current (workspace--get-name current-idx))
              (current-frame (exwm-workspace--workspace-from-frame-or-index current-idx))
              (new-frame (exwm-workspace--workspace-from-frame-or-index index)))
         (setf (nth current-idx workspace--workspaces) (read (concat ":" name)))
         (setf (nth index workspace--workspaces) (read (concat ":" current)))
         (exwm-workspace-swap new-frame current-frame))
       (workspace--update-bar))

(defun workspace--switch-by-index (index) "Try to switch to workspace at INDEX."
       (catch 'body
         (if (eq index nil) (throw 'body nil)
           (let* ((max (- (length workspace--workspaces) 1)))
             (if (> index max)
                 (progn (message (format "Workspace %s doesn't exist." (+ 1 index)))
                        (throw 'body nil))
                 (exwm-workspace-switch index))))))

(defun workspace-switch (name) "Switch to workspace named NAME."
       (interactive
        (list (completing-read "Workspace to switch to: "
                               (remove (workspace--get-current) (workspace--names)))))
       (workspace--switch-by-index (workspace--get-index name)))

(defun workspace--get-bar () "Return workspace string for Polybar."
       (modus-themes-with-colors (let*
            ((base-format (concat "%%{F" fg-alt "}%%{T2}%s%%{T-}%%{F-} %s"))
             (focused-format "%%{T4}%s%%{T-}")
             (unfocused-format (concat "%%{T3}%%{F" fg-unfocused "}%s%%{F-}%%{T-}"))
             (tab-list (mapcar (lambda (i) (cdr (assoc 'name i))) (tab-bar-tabs))))
          (concat (string-join
                   (modus-themes-with-colors
                     (let* ((current exwm-workspace-current-index)
                            (workspace-index 0))
                       (cl-loop for ws in workspace--workspaces
                                collect (progn
                                          (setq workspace-index (+ workspace-index 1))
                                          (format base-format
                                                  workspace-index
                                                  (if (eq (cl-position ws workspace--workspaces) current)
                                                      (format focused-format (substring (symbol-name ws) 1))
                                                    (format unfocused-format (substring (symbol-name ws) 1)))))))) " ")
                  (if (/= 1 (length tab-list)) (concat " %{T3}→%{T-} "
                                (string-join
                                 (let* ((current-tab-name (cdr (assoc 'name (tab-bar--current-tab))))
                                        (tab-index 0))
                                   (cl-loop for tab in tab-list
                                            collect (progn
                                                      (setq tab-index (+ tab-index 1))
                                                      (format base-format
                                                              tab-index
                                                              (if (equal tab current-tab-name)
                                                                  (format focused-format tab)
                                                                (format unfocused-format tab)))))) " ")) nil)))))

(with-eval-after-load 'tab-bar
  (let ((l (lambda (&rest m) (workspace--update-bar))))
    (advice-add 'tab-bar-switch-to-tab :after l)
    (advice-add 'tab-bar-switch-to-next-tab :after l)
    (advice-add 'tab-bar-switch-to-prev-tab :after l)
    (advice-add 'tab-bar-new-tab :after l)
    (advice-add 'tab-bar-close-tab :after l)
    (advice-add 'tab-bar-close-other-tabs :after l)
    (advice-add 'tab-bar-rename-tab :after l)))

(defun workspace--update-bar () "Update Polybar workspace display."
       (start-process-shell-command "update-bar" nil "polybar-msg hook exwm 1"))

(use-package desktop-environment
  :demand t
  :after (exwm)
  :diminish desktop-environment-mode
  :init
  (setq desktop-environment-brightness-get-command "light"
        desktop-environment-brightness-set-command ""
        desktop-environment-brightness-get-regexp "^\\([0-9]+\\)"
	desktop-environment-screenshot-command "maim > \"screenshot-$(date +%Y%m%s-%H%M%S).png\""
	desktop-environment-screenshot-partial-command "maim -s > \"screenshot-$(date +%Y%m%s-%H%M%S).png\""
	desktop-environment-screenshot-directory "~/usr/vis/"
	desktop-environment-update-exwm-global-keys :global)
  :config
  (desktop-environment-mode t))

(defun +ednc-display-notification-popup-h (old new)
  (let ((name (format " *notification-%d*" (ednc-notification-id (or old new)))))
    (with-current-buffer (get-buffer-create name)
      (if new (let ((inhibit-read-only t))
                (if old (erase-buffer) (ednc-view-mode))
                (insert (ednc-format-notification new t))
                (when (posframe-workable-p)
                  (let* ((orfn (symbol-function 'posframe--get-font-height)))
                    (defun posframe--get-font-height (_) "" 10)
                    (posframe-show
                     name
                     :font "Recursive Sans Linear Static"
                     :font-height 10
                     :parent-frame nil
                     :timeout 4
                     :internal-border-width 1
                     :internal-border-color (modus-themes-color 'fg-unfocused)
                     :min-width 30
                     :min-height 4
                     :z-group 'above
                     :poshandler #'posframe-handler-exwm-top-right)
                    (fset 'posframe--get-font-height orfn))))))
    (run-with-timer 8 nil #'kill-buffer name)))

(use-package exwm-systemtray
  :demand t)
(use-package exwm-config)
(use-package ednc
  :demand t
  :after (exwm)
  :diminish ednc-mode
  :init
  (add-hook 'ednc-notification-presentation-functions #'+ednc-display-notification-popup-h)
  :config (ednc-mode t))
(use-package exwm
  :demand t
  :after (exwm-config exwm-systemtray)
  :hook ((exwm-init . +exwm-init-h)
         (exwm-workspace-switch . workspace--update-bar))
  :init
  (setq exwm-workspace-number (length workspace--workspaces)
        mouse-autoselect-window t
        focus-follows-mouse t
        display-time-default-load-average nil)
  :config
  (exwm-systemtray-enable)
  (add-hook 'exwm-update-class-hook
            (lambda () (exwm-workspace-rename-buffer exwm-class-name)))
  (setq exwm-input-global-keys
        `(([?\s-r] . exwm-reset)
          ([?\s-w] . exwm-workspace-switch)
          ([?\s-&] . (lambda (command)
                       (interactive (list (read-shell-command "% ")))
                       (start-process-shell-command command nil command)))
          ,@(mapcar (lambda (i)
                      `(,(kbd (format "s-%d" i)) .
                        (lambda () (interactive)
                          (workspace--switch-by-index ,(- i 1)))))
                    (number-sequence 1 9))
          ([?\s-0] . (lambda () (interactive) (workspace--switch-by-index 10)))))
  (setq exwm-input-simulation-keys
        '(([?\C-b] . [left])
          ([?\C-f] . [right])
          ([?\C-p] . [up])
          ([?\C-n] . [down])
          ([?\C-a] . [home])
          ([?\C-e] . [end])
          ([?\M-v] . [prior])
          ([?\C-v] . [next])
          ([?\C-d] . [delete])
          ([?\C-k] . [S-end delete])))
  (exwm-enable))
