(use-package magit
  :diminish auto-revert-mode
  :bind (("C-z g" . magit-status)))
