{ config, pkgs, ... }:
{
  home = {
    packages = [
      pkgs.ag
    ];
    sessionVariables = {
      EDITOR = "emacsclient";
      IRLITE_DIR = "$HOME/src/irlite.d";
    };
  };

  programs.emacs = {
    enable = true;
    package = pkgs.emacsGcc; /* .override {
      withGTK2 = false;
      withGTK3 = true;
      withXwidgets = true;
    }; */
    extraPackages = epkgs: with epkgs; [
      # base
      use-package
      diminish
      no-littering
      gcmh
      async

      # (plc/layer :exwm)
      evil
      exwm
      desktop-environment
      ednc
      
      # (plc/layer :selectrum)
      selectrum
      prescient
      selectrum-prescient
      orderless
      marginalia
      embark
      mini-frame
      consult

      racket-mode
      
      pkgs.mu
      hydra
      avy
      projectile
      direnv
      which-key
      which-key-posframe
      lsp-mode
      company
      company-posframe
      treemacs
      treemacs-projectile
      flycheck
      elnode
      magit
      treemacs-magit
      edwina
      posframe
      vterm
      modus-themes
      nix-mode elixir-mode
      zig-mode
      web-mode
      impatient-mode
      lispy
      auctex
      slime slime-company
      circe
      circe-notifications
      auctex
    ];
    overrides = self: super: {
      auctex = super.elpaPackages.auctex;
      exwm = super.exwm;
    };
  };
}
