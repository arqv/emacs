(use-package selectrum
  :demand t
  :diminish selectrum-mode
  :config
  (selectrum-mode t))

(use-package marginalia
  :demand t
  :diminish marginalia-mode
  :bind (:map minibuffer-local-map
              ("M-A" . marginalia-cycle))
  :config
  (marginalia-mode t)
  :init
  (setq marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
  (advice-add #'marginalia-cycle :after
              (lambda () (when (bound-and-true-p selectrum-mode) (selectrum-exhibit)))))

(use-package mini-frame
  :custom (mini-frame-standalone
           t
           mini-frame-color-shift-step
           10
           mini-frame-show-parameters
           '((top . 32)
             (width . 0.7)
             (left . 0.5))))

(use-package consult
  :bind (("C-s" . consult-line)
         ("C-x b" . consult-buffer)))

(use-package prescient)

(use-package selectrum-prescient
  :demand t
  :after (selectrum prescient)
  :config
  (selectrum-prescient-mode t))

(use-package orderless
  :demand t
  :after (selectrum)
  :config
  ;; only use orderless for selectrum completions
  (setq selectrum-refine-candidates-function #'orderless-filter
        selectrum-highlight-candidates-function #'orderless-highlight-matches))

(eshell-command-result "ls")
