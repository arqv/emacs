;;; nm --- NetworkManager utilities for Emacs  -*- lexical-binding: t; -*-

;;; Commentary:
;; Implemented features:
;; - Activate profile
;; - Connect to new access point

;;; Code:
(defsubst nm--cmd (args &optional split) "Call `nmcli' with ARGS and split if SPLIT is non-nil."
       (let* ((cmd (concat "nmcli " args))
              (output (shell-command-to-string cmd)))
         (if split (split-string output "\n" t " +")
           (string-trim output))))

(defun nm-get-profiles (&optional active type) "Get profiles filtering by active if ACTIVE is non-nil."
       (mapcar (lambda (i) (let ((s (split-string i ":"))) (cons (nth 1 s) (nth 0 s))))
               (nm--cmd (concat "-t -f TYPE,NAME connection show" (if active " --active" "")) t)))
(defun nm-activate-profile (profile) "Activate PROFILE and connect to it."
       (interactive
        (list (completing-read "Select profile: " (mapcar (lambda (p) (car p)) (nm-get-profiles)))))
       (message (nm--cmd (format "connection up '%s'" profile))))
(defun nm-deactivate-profile (profile) "Activate PROFILE and connect to it."
       (interactive
        (list (completing-read "Select profile: " (mapcar (lambda (p) (car p)) (nm--get-profiles)))))
       (message (nm--cmd (format "connection down '%s'" profile))))
(defun nm-connect-to (network password) "Connect to access point NETWORK using PASSWORD, creating a new profile."
       (interactive
        (list (completing-read "Network: " (nm/cmd "-t -f SSID device wifi" t))
              (read-string "Password: ")))
       (message (nm--cmd (format "device wifi connect '%s' password '%s'" network password))))
(defun nm-status () "Show connectivity status."
       (interactive)
       (message (nm--cmd "-f state,connectivity,wifi g")))

(provide 'nm)
;;; nm.el ends here
