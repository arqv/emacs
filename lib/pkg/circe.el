(defun +circe-config-h () ""
       (enable-circe-notifications)
       (enable-circe-color-nicks))

(defun irc--get-password (server)
  (auth-source-pass-get 'secret (concat "irc/" server)))

(use-package circe
  :commands circe
  :hook (circe-mode . +circe-config-h)
  :bind (("C-z i" . circe))
  :init
  (defalias 'irc #'circe)
  (setq circe-network-options
        '(("tilde.chat"
           :tls t
           :host "na.tilde.chat"
           :port (6697 . 6697)
           :nick "sergal"
           :sasl-username "nue"
           :sasl-password irc--get-password
           :channels ("#meta")))))
