(use-package treemacs-magit
  :after (treemacs magit))
(use-package treemacs-projectile
  :after (treemacs projectile))
(use-package treemacs
  :commands (treemacs)
  :bind ("C-z d" . treemacs)
  :init
  (setq treemacs-no-png-images t))
