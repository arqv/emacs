(defun +eshell-1st-time-h () "Personal eshell configuration."
       (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
       (add-hook 'eshell-output-filter-functions 'eshell-truncate-buffer)
       (setq eshell-history-size 10000
             eshell-buffer-maximum-lines 1000
             eshell-hist-ignoredups t
             eshell-scroll-to-bottom-on-input t))
(use-package eshell
  :commands eshell
  :hook (eshell-first-time-mode . +eshell-1st-time-h)
  :config
  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t
         eshell-visual-commands '("top"))))
