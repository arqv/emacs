(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(tooltip-mode -1)
(fringe-mode '(8 . 1))

(defun +theme-switch-h () "Modifications to add padding to the modeline."
       (progn
         (modus-themes-with-colors
           (setenv "POLYBAR_BG" bg-main)
           (setenv "POLYBAR_DIM" fg-unfocused)
           (setenv "POLYBAR_FG" fg-main)
           (setenv "POLYBAR_BATTERY_CHARGING"
                   (format "%%{F%s}%%{B%s}[↑%%{T2}<label-charging>%%{T-}]%%{B-}%%{F-}" yellow-nuanced-fg yellow-nuanced-bg))
           (setenv "POLYBAR_BATTERY_FULL"
                   (format "%%{F%s}%%{B%s}[%%{T2}*<label-full>%%{T-}]%%{B-}%%{F-}" green-nuanced-fg green-nuanced-bg))
           (setenv "POLYBAR_BATTERY_DISCHARGING"
                   (format "%%{F%s}%%{B%s}[↓%%{T2}<label-discharging>%%{T-}]%%{B-}%%{F-}" red-nuanced-fg red-nuanced-bg))
           (setenv "POLYBAR_CPU"
                   (format "%%{F%s}%%{B%s}[%%{T4}CPU:%%{T-} <label>]%%{B-}%%{F-}" magenta-nuanced-fg magenta-nuanced-bg))
           (setenv "POLYBAR_MEMORY"
                   (format "%%{F%s}%%{B%s}[%%{T4}RAM:%%{T-} <label>]%%{B-}%%{F-}" blue-nuanced-fg blue-nuanced-bg))
           (setenv "POLYBAR_VOLUME"
                   (format "%%{F%s}%%{B%s}[v%%{T2}<label-volume>%%{T-}]%%{B-}%%{F-}" cyan-nuanced-fg cyan-nuanced-bg))
           (setenv "POLYBAR_VOLUME_MUTED"
                   (format "%%{F%s}[----]%%{F-}" fg-unfocused))
           (setenv "POLYBAR_TAILSCALE"
                   (format "%%{F%s}%%{B%s}[%%{T3}<label>%%{T}]%%{B-}%%{F-}"
                           red-nuanced-fg red-nuanced-bg)))
         (call-process-shell-command "systemctl --user import-environment")
         (call-process-shell-command "systemctl --user restart polybar &")
         (call-process-shell-command "echo \"mode-line\nmode-line\" | ~/stumpish"))
       (let ((p 2))
         (set-face-attribute 'mode-line nil
                             :box `(:line-width ,p :color ,(modus-themes-color 'bg-active)))
         (set-face-attribute 'mode-line-inactive nil
                             :box `(:line-width ,p :color ,(modus-themes-color 'bg-dim)))))

(use-package modus-themes
  :demand t
  :bind (("C-z t" . modus-themes-toggle))
  :hook ((modus-themes-after-load-theme . +theme-switch-h))
  :init
  (setq modus-themes-slanted-constructs t
        modus-themes-bold-constructs t
        modus-themes-fringes nil
        
        modus-themes-mode-line '3d
        modus-themes-syntax 'alt-syntax-yellow-comments
        modus-themes-no-mixed-fonts nil
        modus-themes-prompts 'intense
        modus-themes-completions 'opinionated
        modus-themes-region 'no-extend
        modus-themes-diffs 'desaturated
        modus-themes-lang-checkers 'colored-background
        modus-themes-headings '((t . section))
        modus-themes-scale-headings t
        modus-themes-paren-match 'subtle-bold)
  (load-theme 'modus-operandi t t)
  (load-theme 'modus-vivendi t t)
  :config
  (modus-themes-load-vivendi)
  (+theme-switch-h))
