(use-package auth-source
  :demand t
  :init (setq auth-sources '(password-store)))
(use-package auth-source-pass
  :demand t
  :config (auth-source-pass-enable))

(use-package mu4e
  :commands (mu4e mu4e-update-mail-and-index)
  :bind (("C-z m u" . mu4e-update-mail-and-index)
         ("C-z m d" . mu4e)
         ("C-z m c" . compose-mail))
  :init
  (setq mu4e-maildir "~/usr/mail/"
        mu4e-sent-folder "/merp@mailbox.org/Sent"
        mu4e-drafts-folder "/merp@mailbox.org/Drafts"
        mu4e-trash-folder "/merp@mailbox.org/Trash")
  (setq mu4e-get-mail-command "offlineimap")
  (setq mail-user-agent 'mu4e-user-agent)
  (setq send-mail-function 'smtpmail-send-it
        message-send-mail-function 'smtpmail-send-it)
  (setq starttls-use-gnutls t
        smtpmail-default-smtp-server "smtp.mailbox.org"
        smtpmail-smtp-server "smtp.mailbox.org"
        smtpmail-stream-type 'ssl
        smtpmail-smtp-service 465
        smtpmail-smtp-user "merp@mailbox.org"
        smtpmail-local-domain "mailbox.org")
  (require 'smtpmail)
  (require 'smtpmail-async))
