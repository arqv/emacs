(use-package slime
  :hook ((lisp-mode . lispy-mode)
         (lisp-mode . company-mode))
  :init
  (setq inferior-lisp-program "sbcl")
  (setq slime-lisp-implementations
        `((sbcl ("sbcl"))))
  :config
  (slime-setup '(slime-company)))
