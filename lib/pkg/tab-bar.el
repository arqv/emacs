(use-package tab-bar
  :bind (("C-x t i" . tab-previous)
	 ("C-x t <left>" . tab-previous)
	 ("C-x t <right>" . tab-next))
  :init
  (setq tab-bar-show nil
        tab-bar-close-button-show nil
        tab-bar-new-button-show nil
        tab-bar-new-tab-choice "*scratch*"))
