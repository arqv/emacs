(use-package helm
  :demand t
  :diminish helm-mode
  :bind (("M-x" . helm-M-x)
	 ("C-x C-f" . helm-find-files)
	 ("C-x C-b" . helm-mini))
  :config (helm-mode 1))

(use-package helm-posframe
  :demand t
  :after (helm posframe)
  :init
  (setq helm-posframe-min-width 1000
	helm-posframe-poshandler 'posframe-poshandler-frame-bottom-left-corner
	helm-posframe-border-width 0
	helm-posframe-parameters '((parent-frame nil)))
  :config (helm-posframe-enable))
(use-package helm-projectile
  :after (helm projectile))
(use-package helm-flycheck
  :after (helm flycheck))
(use-package helm-circe
  :after (helm circe))

