(use-package org
  :hook ((org-mode . variable-pitch-mode)
         (org-mode . visual-line-mode)))
