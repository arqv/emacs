(defun pinentry-emacs (desc prompt ok error)
  (let ((str (read-passwd (concat (replace-regexp-in-string "%22" "\"" (replace-regexp-in-string "%0A" "\n" desc)) prompt ": "))))
    str))

(defun reload-path () "Reload Emacs' load path."
       (interactive)
       (setq load-path (read
                        (shell-command-to-string (concat "emacs -batch -eval "
                                                         "'(prin1 load-path)' "
                                                         "2>/dev/null"))))
       (message "`load-path' has been reloaded successfully."))

(use-package ligature
  :config
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("=>" "==" "/=" "!=" ">=" "<="))
  ;; Enables ligature checks globally in all buffers. You can also do it
  ;; per mode with `ligature-mode'.
  (global-ligature-mode t))

(use-package posframe
  :demand t
  :init
  (setq posframe-mouse-banish nil)
  (define-advice posframe-show (:filter-return (frame) exwm-deparent)
    (set-frame-parameter frame 'parent-frame nil)
    frame))

(defun posframe-handler-exwm-top-right (info) ""
       (if (not (featurep 'exwm)) (posframe-poshandler-frame-center info)
         (let* ((workarea (elt exwm-workspace--workareas exwm-workspace-current-index))
                (x (aref workarea 0))
                (y (aref workarea 1))
                (fw (aref workarea 2))
                (fh (aref workarea 3))
                (pw (plist-get info :posframe-width))
                (ph (plist-get info :posframe-height)))
           (cons
            (+ x (- (- fw pw) 8))
            (+ y 8)))))
(defun posframe-handler-exwm-top-center (info) ""
       (if (not (featurep 'exwm)) (posframe-poshandler-frame-center info)
         (let* ((workarea (elt exwm-workspace--workareas exwm-workspace-current-index))
                (x (aref workarea 0))
                (y (aref workarea 1))
                (fw (aref workarea 2))
                (fh (aref workarea 3))
                (pw (plist-get info :posframe-width))
                (ph (plist-get info :posframe-height)))
           (cons
            (+ x (/ (- fw pw) 2))
            y))))

(use-package direnv
  :diminish direnv-mode
  :init (setq direnv-always-show-summary nil)
  :config (direnv-mode))

(use-package flycheck) ;; TODO: setup
(use-package vterm
  :commands vterm)

(progn                                  ;; show-paren-mode configuration
  (setq show-paren-delay 0
        show-paren-when-point-inside-paren t
        show-paren-style 'expression)
  (show-paren-mode t))

(defun plist-walk (plist &rest keys) "Walk through nested PLIST returning the value from the last key in KEYS."
       (setq e `(plist-get plist ,(pop keys)))
       (while keys
	 (setq e `(plist-get ,e ,(pop keys))))
       (eval e))

(defun +prog-mode-h () ""
       (display-line-numbers-mode t)
       (electric-pair-local-mode t)) 
(add-hook 'prog-mode-hook #'+prog-mode-h)
