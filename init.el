;;; package --- init.el  -*- lexical-binding: t; -*-
;;; Commentary:
;; personal Emacs configuration

;;; Code:
(let ((file-name-handler-alist nil))
  (require 'no-littering)
  (require 'notifications)
  (setq custom-file (no-littering-expand-etc-file-name "custom.el"))
  (setq private-file (no-littering-expand-var-file-name "private.el"))
  (setq user-full-name "Ezra Bravo"
        user-mail-address "merp@mailbox.org")
  (add-to-list 'load-path (concat user-emacs-directory "lib"))
  (add-to-list 'load-path (concat user-emacs-directory "lib/self"))
  
  (load custom-file 'noerror)
  
  (setq comp-deferred-compilation t)
  (setq inhibit-startup-screen t)
  (setq make-backup-files nil)
  (setq backup-by-copying t)
  (setq create-lockfiles nil)
  (setq auto-save-default nil)
  (setq-default indent-tabs-mode nil)

  (set-frame-font "TamzenForPowerline-10.5")
  (set-face-font 'variable-pitch "Recursive Sans Linear Static 9")

  (require 'nm)

  (global-unset-key (kbd "C-z"))
  
  (load "pkg/base.el")
  (load "pkg/theme.el")
  (load "pkg/ivy.el")
  ;; (load "pkg/selectrum")
  (load "pkg/projectile.el")
  (load "pkg/org.el")
  ;; Magit is broken in emacs-overlay.
  (load "pkg/magit.el")
  (load "pkg/mu4e.el")
  (load "pkg/company.el")
  (load "pkg/lsp.el")
  (load "pkg/which-key.el")
  (load "pkg/tab-bar.el")
  (load "pkg/eshell.el")
  (load "pkg/circe.el")
  (load "pkg/treemacs.el")
  (load "pkg/dired.el")
  
  (load "lang/zig.el")
  (load "lang/markdown.el")
  (load "lang/emacslisp.el")
  (load "lang/commonlisp.el")
  ;; (load "plc-exwm.el")
  )

(use-package gcmh
  :diminish gcmh-mode
  :demand t
  :config (gcmh-mode 1))

(provide 'init)
;;; init.el ends here

(put 'dired-find-alternate-file 'disabled nil)
